/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"sync"
	"time"
)

var done = false

func main() {
	topic := "topic-cond"
	cond := sync.NewCond(&sync.Mutex{})
	go Consumer(topic, cond)
	go Consumer(topic, cond)
	go Consumer(topic, cond)
	Push(topic, cond)
	time.Sleep(5 * time.Second)
}

func Consumer(topic string, cond *sync.Cond) {
	cond.L.Lock()
	for !done {
		fmt.Println("wait")
		cond.Wait() // 所有等待这个条件的goroutine都会被阻塞住
	}
	fmt.Println("starts Consumer")
	cond.L.Unlock()
}

func Push(topic string, cond *sync.Cond) {
	fmt.Println("starts push")
	cond.L.Lock()
	done = true
	cond.L.Unlock()
	fmt.Println("awake all") // 唤醒阻塞的对象
	cond.Broadcast()
}
