/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package schedule

import (
	"context"
	"testing"
)

func TestNewFIFOScheduler(t *testing.T) {
	s := NewFIFOScheduler()
	defer s.Stop()

	next := 0
	jobCreater := func(i int) Job {
		return func(ctx context.Context) {
			if next != i {
				t.Fatalf("job#%d: got %d, want %d", i, next, i)
			}
			next = i + 1
		}
	}

	var jobs []Job
	for i := 0; i < 100; i++ {
		jobs = append(jobs, jobCreater(i))
	}

	for _, job := range jobs {
		s.Schedule(job)
	}

	s.WaitFinish(100)
	if s.Scheduled() != 100 {
		t.Errorf("scheduled = %d, want %d", s.Scheduled(), 100)
	}
}
